<?php
require_once 'src/php/core/http/Response.php';
require_once 'src/php/core/http/Router.php';
require_once 'src/php/home/controllers/Home.php';
require_once 'src/php/user/controllers/UserController.php';
require_once 'src/php/user/models/User.php';
require_once 'src/php/post/controllers/PostController.php';
require_once 'src/php/comment/controllers/CommentController.php';
require_once 'src/php/user/controllers/ProfileController.php';
require_once 'src/php/user/services/UserService.php';

Router::get('/', function () {
    (new Home())->redirectToMain();
});
Router::get('/register', function () {
    UserController::redirectRegisterPage();
});
Router::post('/register', function (Request $req, Response $res) {
    $responseCode = (new UserController())->registerUser($req->getJSON());
    $res->setResponse($responseCode);
});
Router::post('/sign-in', function (Request $req, Response $res) {
    try {
        $user = ((new UserController())->signInUser($req->getJSON()->email, $req->getJSON()->password));
        $res->setResponse(200, $user);
    } catch (WrongDataException $exception) {
        $res->setResponse(400);
    } catch (NotFoundException $e) {
        $res->setResponse(404);
    }
});
Router::post('/logout', function () {
    (new UserController())->logoutCurrentUser();
});
Router::get('/posts', function (Request $req, Response $res) {
    $posts = (new PostController())->last15Posts();
    if ($posts) {
        $res->setResponse(200, $posts);
    } else {
        $res->setResponse(404);
    }
});
Router::get('/create-post', function () {
    (new PostController())->redirectCreatePost();
});
Router::post('/post', function (Request $req, Response $res) {
    $responseCode = ((new PostController())->createPost());
    $res->setResponse($responseCode);
});
Router::post('/comment', function (Request $req, Response $res) {
    try {
        $comment = ((new CommentController())->createComment($req->getJSON()->text, $req->getJSON()->id_post));
        $res->setResponse(201, $comment);
    } catch (NotFoundException $e) {
        $res->setResponse(404);
    }
});
Router::get("/profile/id=?", function (Request $req) {
    (new ProfileController())->redirectProfilePage($req->getParamId());
});
Router::get("/user/id=?", function (Request $req, Response $res) {
    try {
        $user = (new UserController())->findUserById($req->getParamId());
        $res->setResponse(200, $user);
    } catch (NotFoundException $e) {
        $res->setResponse(404);
    }
});
Router::get("/posts/user_id=?", function (Request $req, Response $res) {
    $posts = (new PostController())->findPostsByUserId($req->getParamId());
    $res->setResponse(200, $posts);
});
Router::get("/search/pattern=?", function (Request $req, Response $res) {
    $posts = (new UserController())->searchUser($req->param);
    $res->setResponse(200, $posts);
});
Router::run();