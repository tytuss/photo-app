async function getUserById(userId) {
    const param = new URLSearchParams({
        id: userId
    });
    const url = `/user/${param}`
    let response = await fetch(url, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
        }
    });
    return response.json();
}

async function getPostsByUserId(userId) {
    const param = new URLSearchParams({
        user_id: userId
    });
    const url = `/posts/${param}`
    let response = await fetch(url, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
        }
    });
    return response.json();
}

function loadContent() {
    const userId = window.location.pathname.split("=")[1];
    getUserById(userId).then(user => {
        const profile = document.querySelector('#profile_img');
        profile.src = `${window.location.origin}/${user.photo}`;
        const name = document.querySelector('.profile_name')
        name.innerText = `${user.name} ${user.surname}`
    })
    getPostsByUserId(userId).then(posts => {
        console.log(posts);
        posts.forEach(p => renderPost(p));
        modifyNumberPosts(posts.length);
    })
}

function modifyNumberPosts(postsLength) {
    const numberPosts = document.querySelector('.profile_count_posts')
    numberPosts.innerText = `Liczba postów: ${postsLength}`;
}

function renderPost(post) {
    const postElement = document.querySelector('.profile_post');
    const hasPostId = postElement.hasAttribute('id_post');
    if (!hasPostId) {
        document.querySelector(".posts").removeChild(postElement);
    }
    const clonedPost = postElement.cloneNode(true);
    clonedPost.style.visibility = "visible";
    clonedPost.setAttribute("id_post", post.id_post);
    const postImg = clonedPost.querySelector('#post_img');
    postImg.src = `${window.location.origin}/${post.photo}`;
    document.querySelector(".posts").appendChild(clonedPost);
}