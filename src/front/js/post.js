document.querySelector('#create-post-form').addEventListener('submit', (event) => {
    event.preventDefault();
    const files = event.target.querySelector('[name=filename]').files;
    const description = event.target.querySelector('[name=describe_photo]').value;
    const formData = new FormData()
    formData.append('file', files[0])
    formData.append("description", description);
    fetch('/post', {
        method: 'POST',
        body: formData
    }).then(response => {
        if (response.ok) {
            alert("pomyślnie dodano post!")
        }
    });
});