document.querySelector('#register-form').addEventListener('submit', (event) => {
    event.preventDefault();
    const email = event.target.querySelector('[name=email]').value;
    const password = event.target.querySelector('[name=password]').value;
    const name = event.target.querySelector('[name=name]').value;
    const surname = event.target.querySelector('[name=surname]').value;
    const phone = event.target.querySelector('[name=phone]').value;
    fetch("/register", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email,
            password,
            name,
            surname,
            phone
        })
    }).then(response => validateRegisterResponse(response))
});

function validateRegisterResponse(response) {
    if (response.ok) {
        alert("pomyslnie zarejestrowano użytkownika")
    } else if (response.status === 400) {
        errorMessage("email jest już zajęty lub nieprawidłowe dane")
    }
}

function errorMessage(message) {
    document.querySelector('.register-error-message').innerHTML = message;
}

document.querySelector('#button-login').addEventListener('click', () => {
    window.location.pathname = '/'
})