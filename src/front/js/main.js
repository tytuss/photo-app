async function loadPosts() {
    fetch("/posts", {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
        }
    }).then(response => response.json()).then(response => {
        response.forEach(post => {
            console.log(post);
            renderPost(post);
        });
    })
}

async function renderPost(post) {
    const parent = document.createElement('div');
    const photosSection = document.querySelector('.photos')
    parent.setAttribute("id_post", post.id_post);
    photosSection.appendChild(parent);

    createHeaderSection(parent, post.author);
    createPhotoSection(parent, post);
    createCommentsSection(parent, post.comments);
    createInsertCommentSection(parent);
}

function insertCommentClick(event) {
    event.preventDefault();
    const parent = event.target.parentElement.parentElement;
    const id_post = parent.getAttribute("id_post");
    const input = event.target.querySelector('[name=add-comment-field]');
    const text = input.value;
    input.value = "";

    fetch("/comment", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            id_post,
            text
        })
    }).then(result => result.json())
        .then(result => {
            insertNewComment(result)
        })
}

function insertNewComment(comment) {
    addCommentSection(comment);
}

function createChildDiv(className, html) {
    const elementDiv = document.createElement('div');
    elementDiv.className = className;
    if (html !== undefined) {
        elementDiv.innerHTML = html;
    }
    return elementDiv;
}

function createHeaderSection(parent, author) {
    const header = createChildDiv("header-photo");
    header.addEventListener('click', () => {
        window.location.pathname = `/profile/id=${author.id}`;
    });
    const container = document.createElement('div');
    const img = document.createElement('img');
    img.src = author.photo;
    img.className = "circular--square"
    const p = document.createElement('p');
    p.innerText = `${author.name} ${author.surname}`;
    header.appendChild(container);
    container.appendChild(img);
    container.appendChild(p);
    parent.insertBefore(header, parent.children[0]);
}

function createPhotoSection(parent, post) {
    const photo = createChildDiv("photo-style");
    parent.insertBefore(photo, parent.children[1]);
    const img = document.createElement('img');
    img.src = post.photo;
    photo.appendChild(img);
}

function createInsertCommentSection(parent) {
    const htmlForm = `<form class='form-add-comment'>
                        <input name='add-comment-field' type='text' placeholder='Dodaj komentarz'>
                        <input type='submit' value='DODAJ'>
                    </form>`;

    const addComment = createChildDiv("insert-comment", htmlForm);
    parent.insertBefore(addComment, parent.children[3]);
    addComment.querySelector('.form-add-comment').addEventListener('submit', (e) => insertCommentClick(e));
}

function createCommentsSection(parent, comments) {
    const commentsContainer = createChildDiv("photo-comments");
    parent.insertBefore(commentsContainer, parent.children[2]);
    comments.forEach(c => addCommentSection(c));
}

function addCommentSection(comment) {
    const p = document.createElement('p');
    p.addEventListener('click', () => {
        window.location.pathname = `/profile/id=${comment.author.id}`;
    });
    p.className = "comment";
    p.innerText = `${comment.author.name} ${comment.author.surname} : ${comment.message}`;
    const parent = getPostParentElement(comment.id_post);
    const comments = parent.querySelector('.photo-comments');
    comments.appendChild(p);
}

function getPostParentElement(idPost) {
    return document.querySelector(`[id_post="${idPost}"]`);
}
