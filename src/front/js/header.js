document.querySelector('.nav-profile').addEventListener('click', (event) => {
    const dropDown = document.querySelector('.dropdown-content');
    if (dropDown.style.display === "block") {
        dropDown.style.display = "none";
    } else {
        dropDown.style.display = "block";
    }
})
document.querySelector('#my-profile-button').addEventListener('click', () => {
    const user = JSON.parse(localStorage.getItem("user"));
    window.location.pathname = `/profile/id=${user.id}`;
});
document.querySelector('#logout-button').addEventListener('click', () => {
    logout();
});
document.querySelector('[name=user-search]').addEventListener('keyup', (e) => {
    const pattern = document.querySelector('[name=user-search]');
    const searchResultElement = document.querySelector('.search-bar-result');
    findUsersByPattern(pattern.value).then(users => {
        removeChildElements(searchResultElement);
        return users;
    }).then(users => users.forEach(user => {
        const a = document.createElement('a');
        a.innerText = `${user.name} ${user.surname}`;
        a.setAttribute("id_user", user.id);
        a.addEventListener('click', (e) => {
            const id = e.target.getAttribute('id_user');
            window.location.pathname = `/profile/id=${id}`;
        })
        searchResultElement.append(a);
    }));
});

function removeChildElements(parent) {
    while (parent.firstChild) {
        parent.firstChild.remove()
    }
}

function logout() {
    fetch("/logout", {
        method: "POST"
    }).then(() => {
        localStorage.removeItem("user");
        reloadPage();
    })
}

async function findUsersByPattern(pattern) {
    const param = new URLSearchParams({
        pattern: pattern
    });
    const url = `/search/${param}`
    let response = await fetch(url, {
        method: "GET",
        headers: {
            'Content-Type': 'application/json',
        }
    });
    return response.json();
}

function reloadPage() {
    window.location.reload(true);
}
document.querySelector('.nav-create-post').addEventListener('click', () => {
    window.location.pathname = '/create-post'
});
document.querySelector('.nav-home').addEventListener('click', () => {
    window.location.pathname = '/'
});