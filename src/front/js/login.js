document.querySelector('#button-register').addEventListener('click', () => {
    window.location.pathname = '/register'
})

document.querySelector('#login-form').addEventListener('submit', (event) => {
    event.preventDefault();
    const email = event.target.querySelector('[name=email]').value;
    const password = event.target.querySelector('[name=password]').value;

    fetch("/sign-in", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email,
            password,
        })
    }).then(response => validateSignInResponse(response));
});

async function validateSignInResponse(response) {
    if (response.status === 404) {
        errorMessage("Nie istnieje taki użytkownik. Spróbuj ponownie.");
    } else if (response.status === 400) {
        errorMessage("Nieprawidłowe dane logowania. Spróbuj ponownie.");
    } else if (response.ok) {
        const user = await response.json();
        localStorage.setItem("user", JSON.stringify(user));
        reloadPage();
    }
}

function errorMessage(message) {
    document.querySelector('.login-error-message').innerHTML = message;
}

function reloadPage() {
    window.location.reload(true);
}

