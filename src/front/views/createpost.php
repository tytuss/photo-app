<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="src/front/css/style.css" rel="stylesheet" type="text/css">
    <link href="src/front/css/post.css" rel="stylesheet" type="text/css">
    <link href="src/front/css/header.css" rel="stylesheet" type="text/css">
    <title>create photo post</title>
</head>
<body>
<div class="container-index">
    <main>
        <?php include('src/front/views/header.php'); ?>
        <section class="create-post">
            <div class="container-create-post">
                <form id="create-post-form">
                    <input name="describe_photo" placeholder="opis do zdjęcia" type="text">
                    <input name="filename" type="file">
                    <input type="submit" value="dodaj zdjęcie">
                </form>
            </div>
        </section>
    </main>
</div>
<script crossorigin="anonymous" src="https://kit.fontawesome.com/39c2625bfe.js"></script>
<script src="src/front/js/post.js"></script>
<script src="src/front/js/header.js"></script>
</body>
</html>