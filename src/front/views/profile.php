<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="../src/front/css/style.css" rel="stylesheet" type="text/css">
    <link href="../src/front/css/post.css" rel="stylesheet" type="text/css">
    <link href="../src/front/css/header.css" rel="stylesheet" type="text/css">
    <link href="../src/front/css/profile.css" rel="stylesheet" type="text/css">
    <title>profile photo-app</title>
<body onload="loadContent();">
<div class="container-profile">
    <main>
        <?php include('src/front/views/header.php'); ?>
        <section class="profile">
            <img alt="no photo" class="circular--square" id="profile_img">
            <p class="profile_name"></p>
            <p class="profile_count_posts">Liczba postów</p>
        </section>
        <section class="posts">
            <div class="profile_post">
                <img alt="no photo" id="post_img">
            </div>
        </section>
    </main>
</div>
<script crossorigin="anonymous" src="https://kit.fontawesome.com/39c2625bfe.js"></script>
<script src="../src/front/js/header.js"></script>
<script src="../src/front/js/profile.js"></script>
</body>
</html>