<header>
    <div class="search-bar" onmouseover="hideDropDownUser()">
        <input id="user-search-input" name="user-search" placeholder="szukaj użytkownika" type="search">
        <div class="search-bar-result">
        </div>
    </div>
    <div class="nav-buttons">
        <div class="nav-home">
            <i class="fas fa-home"></i>
        </div>
        <div class="nav-create-post">
            <i class="fas fa-plus"></i>
        </div>
        <div class="nav-profile">
            <i class="fas fa-user"></i>
            <div class="dropdown-content">
                <a id="my-profile-button">Mój profil</a>
                <a id="logout-button">Wyloguj</a>
            </div>
        </div>
    </div>
</header>

