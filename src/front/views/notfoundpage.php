<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="../src/front/css/style.css" rel="stylesheet" type="text/css">
    <link href="../src/front/css/header.css" rel="stylesheet" type="text/css">
    <title>not found page photo-app</title>
<body>
<div class="not-found-container">
    <main>
        <?php include('src/front/views/header.php'); ?>
        <section class="not-found-page">
            Strona nie istnieje
        </section>
    </main>
</div>
<script crossorigin="anonymous" src="https://kit.fontawesome.com/39c2625bfe.js"></script>
<script src="../src/front/js/header.js"></script>
</body>
</html>