<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="src/front/css/style.css" rel="stylesheet" type="text/css">
    <link href="src/front/css/post.css" rel="stylesheet" type="text/css">
    <link href="src/front/css/header.css" rel="stylesheet" type="text/css">
    <title>photo-app</title>
</head>
<body onload="loadPosts();">
<div class="container-index">
    <main>
        <?php include('src/front/views/header.php'); ?>
        <section class="photos">
        </section>
    </main>
</div>
<script crossorigin="anonymous" src="https://kit.fontawesome.com/39c2625bfe.js"></script>
<script src="src/front/js/main.js"></script>
<script src="src/front/js/header.js"></script>
</body>
</html>