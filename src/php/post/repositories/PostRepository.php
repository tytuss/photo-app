<?php

require_once 'src/php/core/database/Repository.php';
require_once 'src/php/core/exceptions/NotFoundException.php';

class PostRepository extends Repository
{
    private UserRepository $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function addPost(Post $post)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.post (id_user, photo, post_describe, created_date)
            VALUES (:id_user, :photo, :post_describe, :created_date)
        ');
        $id = $post->getAuthor()->getId();
        $photo = $post->getPhoto();
        $postDescribe = $post->getPostDescribe();
        $createdDate = $post->getCreatedDate();
        $stmt->bindParam(':id_user', $id);
        $stmt->bindParam(':photo', $photo);
        $stmt->bindParam(':post_describe', $postDescribe);
        $stmt->bindParam(':created_date', $createdDate);
        $stmt->execute();
    }

    public function loadLast15Posts(): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.post as p
                INNER JOIN public.user as u ON p.id_user = u.id_user
                INNER JOIN public.user_details as ud ON u.id_user_details = ud.id_user_details
                    ORDER BY p.created_date DESC LIMIT 15;
        ');
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findPostsByUserId(int $userId): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.post as p
                INNER JOIN public.user as u ON p.id_user = u.id_user
                INNER JOIN public.user_details as ud ON u.id_user_details = ud.id_user_details
                    WHERE u.id_user = :id_user
                    ORDER BY p.created_date DESC;
        ');
        $stmt->bindParam(':id_user', $userId);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}