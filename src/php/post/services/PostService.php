<?php

require_once 'src/php/post/models/Post.php';
require_once 'src/php/user/services/UserService.php';
require_once 'src/php/post/repositories/PostRepository.php';
require_once 'src/php/core/exceptions/WrongDataException.php';

class PostService
{
    const MAX_FILE_SIZE = 1024 * 1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = 'src/front/img/uploads/';

    private UserService $userService;
    private PostRepository $postRepository;
    private CommentRepository $commentRepository;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->postRepository = new PostRepository();
        $this->commentRepository = new CommentRepository();
    }

    public function last15Posts(): array
    {
        $posts = $this->postRepository->loadLast15Posts();
        return $this->mapDbPostsToModel($posts);
    }

    public function mapDbCommentsToModel($dbComments): array
    {
        $comments = array();
        foreach ($dbComments as $row) {
            $author = $this->userService->mapUserFromDbToModel($row);
            $comment = new Comment($row["id_post"], $author, $row["message"], $row["created_date"]);
            $comments[] = $comment;
        }
        return $comments;
    }

    public function findPostsByUserId(int $userId): array
    {
        $posts = $this->postRepository->findPostsByUserId($userId);
        return $this->mapDbPostsToModel($posts);
    }

    function mapDbPostsToModel($posts): array
    {
        $result = array();
        foreach ($posts as $row) {
            $comments = $this->mapDbCommentsToModel($this->commentRepository->findLast15ByPostId($row["id_post"]));
            $author = $this->userService->mapUserFromDbToModel($row);
            $post = new Post($row["id_post"], $row["photo"], $row["post_describe"], $row["created_date"], $author, $comments);
            $result[] = $post;
        }
        return $result;
    }

    /**
     * @throws NotFoundException
     * @throws WrongDataException
     */
    public function uploadFileAndCreatePost()
    {
        if (is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])) {
            $extension = pathinfo(basename($_FILES['file']["name"]), PATHINFO_EXTENSION);
            $newRandomFileName = uniqid() . "." . $extension;
            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                self::UPLOAD_DIRECTORY . $newRandomFileName
            );
            $fileWithPath = self::UPLOAD_DIRECTORY . $newRandomFileName;
            $description = $_POST['description'];
            $user = $this->userService->getUserFromSession();
            $post = new Post(null, $fileWithPath, $description, $this->getTimeNow(), $user, array());
            $this->postRepository->addPost($post);
        } else {
            throw new WrongDataException("wrong file");
        }
    }

    private function validate(array $file): bool
    {
        if ($file['size'] > self::MAX_FILE_SIZE) {
            return false;
        }

        if (!isset($file['type']) || !in_array($file['type'], self::SUPPORTED_TYPES)) {
            return false;
        }
        return true;
    }

    private function getTimeNow(): string
    {
        return date('Y-m-d H:i:s', time());
    }

}