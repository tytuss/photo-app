<?php

require_once 'src/php/post/services/PostService.php';
require_once 'src/php/user/services/UserService.php';
require_once 'src/php/core/exceptions/NotFoundException.php';
require_once 'src/php/core/exceptions/WrongDataException.php';

class PostController
{
    private PostService $postService;
    private UserService $userService;
    private Response $response;

    public function __construct()
    {
        $this->postService = new PostService();
        $this->userService = new UserService();
        $this->response = new Response();
    }

    public function redirectCreatePost()
    {
        if ($this->userService->isUserLogged()) {
            $this->response->renderHtmlFromPhp("src/front/views/createpost.php");
        } else {
            echo file_get_contents("src/front/views/login.html", true);
        }
    }

    public function last15Posts(): array
    {
        return $this->postService->last15Posts();
    }

    public function findPostsByUserId(int $userId): array
    {
        return $this->postService->findPostsByUserId($userId);
    }

    public function createPost(): int
    {
        try {
            $this->postService->uploadFileAndCreatePost();
            return 201;
        } catch (NotFoundException $exception) {
            return 404;
        } catch (WrongDataException $e) {
            return 400;
        }
    }
}