<?php


class Post implements JsonSerializable
{
    private ?int $id;
    private string $photo;
    private string $postDescribe;
    private string $createdDate;
    private User $author;
    private array $comments;

    /**
     * Post constructor.
     * @param string $photo
     * @param string $postDescribe
     * @param string $createdDate
     * @param User $author
     */
    public function __construct(?int $id, string $photo, string $postDescribe, string $createdDate, User $author, array $comments)
    {
        $this->id = $id;
        $this->photo = $photo;
        $this->postDescribe = $postDescribe;
        $this->createdDate = $createdDate;
        $this->author = $author;
        $this->comments = $comments;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPhoto(): string
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto(string $photo): void
    {
        $this->photo = $photo;
    }

    /**
     * @return string
     */
    public function getPostDescribe(): string
    {
        return $this->postDescribe;
    }

    /**
     * @param string $postDescribe
     */
    public function setPostDescribe(string $postDescribe): void
    {
        $this->postDescribe = $postDescribe;
    }

    /**
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->createdDate;
    }

    /**
     * @param string $createdDate
     */
    public function setCreatedDate(string $createdDate): void
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author): void
    {
        $this->author = $author;
    }

    /**
     * @return array
     */
    public function getComments(): array
    {
        return $this->comments;
    }

    /**
     * @param array $comments
     */
    public function setComments(array $comments): void
    {
        $this->comments = $comments;
    }


    public function jsonSerialize(): array
    {
        return [
            'id_post' => $this->id,
            'photo' => $this->photo,
            'post_describe' => $this->postDescribe,
            'author' => array("id" => $this->author->getId(),
                "name" => $this->author->getName(),
                "surname" => $this->author->getSurname(),
                "photo" => $this->author->getPhoto()),
            'comments' => $this->comments
        ];
    }
}