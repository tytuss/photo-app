<?php


class Response
{
    public function setResponse(int $code, $data = "")
    {
        http_response_code($code);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function renderHtmlFromPhp(string $path)
    {
        $file = file_get_contents($path);
        $content = eval("?>$file");
        echo $content;
    }
}