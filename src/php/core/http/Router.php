<?php

require_once 'Request.php';
require_once 'Response.php';

class Router
{
    private static array $postRoutes = array();
    private static array $getRoutes = array();

    public static function get($route, $callback)
    {
        self::$getRoutes[$route] = $callback;
    }

    public static function post($route, $callback)
    {
        self::$postRoutes[$route] = $callback;
    }

    public static function run()
    {
        $url = $_SERVER['REQUEST_URI'];
        $action = explode("/", $url)[1];
        $action = (stripos($action, "/") !== 0) ? "/" . $action : $action;
        $param = explode("/", $url)[2];
        $paramVal = "";

        if (!empty($param)) {
            $action = $action . "/" . explode("=", $param)[0] . "=?";
            $paramVal = explode("=", $param)[1];
        }

        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') === 0) {
            if (array_key_exists($action, self::$postRoutes)) {
                try {
                    $callback = self::$postRoutes[$action];
                    $callback(new Request($paramVal), new Response());
                } catch (Throwable $exception) {
                    self::logError($exception);
                }
            } else {
                http_response_code(404);
                die("not found routing for path: " . $action);
            }
        }
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'GET') === 0) {
            if (array_key_exists($action, self::$getRoutes)) {
                try {
                    $callback = self::$getRoutes[$action];
                    $callback(new Request($paramVal), new Response());
                } catch (Throwable $exception) {
                    self::logError($exception);
                }
            } else {
                http_response_code(404);
                die("not found routing for path: " . $action);
            }
        }
    }

    private static function logError(Throwable $exception)
    {
        http_response_code(500);
        error_log("--------ERROR-----------", 0);
        error_log($exception->getMessage(), 0);
        error_log($exception, 0);
        echo 'Something go wrong (500), check console nginx';
    }
}