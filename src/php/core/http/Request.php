<?php


class Request
{
    public string $reqMethod;
    public string $contentType;
    public string $param;

    public function __construct(string $param)
    {
        $this->param = $param;
        $this->reqMethod = trim($_SERVER['REQUEST_METHOD']);
        $this->contentType = !empty($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
    }

    public function getJSON()
    {
        if ($this->reqMethod !== 'POST') {
            return [];
        }

        if (strcasecmp($this->contentType, 'application/json') !== 0) {
            return [];
        }
        $content = trim(file_get_contents("php://input"));
        return json_decode($content);
    }

    public function getParamId(): int
    {
        return intval($this->param);
    }
}