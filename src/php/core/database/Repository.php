<?php

require_once 'PostgresDb.php';

class Repository
{
    protected PostgresDb $database;

    public function __construct()
    {
        $this->database = new PostgresDb();
    }
}