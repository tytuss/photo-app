<?php

require_once 'src/php/user/services/UserService.php';

class Home
{
    private Response $response;
    private UserService $userService;
    public function __construct()
    {
        $this->response = new Response();
        $this->userService = new UserService();
    }

    public function redirectToMain()
    {
        if ($this->userService->isUserLogged()) {
            $this->response->renderHtmlFromPhp("src/front/views/main.php");
        } else {
            echo file_get_contents("src/front/views/login.html", true);
        }
    }
}
