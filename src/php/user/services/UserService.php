<?php
require_once 'src/php/core/exceptions/WrongDataException.php';
require_once 'src/php/user/models/User.php';

session_start();

class UserService
{
    private UserRepository $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    /**
     * @throws NotFoundException
     */
    public function getUserFromSession(): User
    {
        if ($this->isUserLogged()) {
            return $this->userRepository->findByEmail($_SESSION['email']);
        }
        throw new NotFoundException();
    }

    public function mapUserFromDbToModel($temp): User
    {
        return new User($temp['id_user'], $temp['email'], $temp['password'], $temp['name'], $temp['surname'], $temp['phone'], $temp["photo_profile"]);
    }

    public function mapUsersFromDbToModel($users): array
    {
        $result = array();
        foreach ($users as $row) {
            $user = $this->mapUserFromDbToModel($row);
            $result[] = $user;
        }
        return $result;
    }

    public function isUserLogged(): bool
    {
        if ($_SESSION['email']) {
            return true;
        }
        return false;
    }

    public function logout()
    {
        session_unset();
        session_destroy();
    }

    /**
     * @throws WrongDataException
     * @throws NotFoundException
     */
    public function authorizeUser(string $email, string $password): User
    {
        if (!$this->isEmailValid($email)) {
            throw new WrongDataException("bad email format");
        }
        $user = $this->userRepository->findByEmail($email);
        if (!password_verify($password, $user->getPassword())) {
            throw new WrongDataException("bad password");
        }
        $this->setUserSession($user->getEmail());
        return $user;
    }

    /**
     * @throws WrongDataException
     * @throws Exception
     */
    public function createUser($userDto)
    {
        if (!$this->isEmailValid($userDto->email)) {
            throw new WrongDataException("bad email format");
        }
        if (empty($userDto->password)) {
            throw new WrongDataException("empty password");
        }
        if (!$this->isEmailTaken($userDto->email)) {
            $user = new User(null,
                $userDto->email,
                $this->getHashPassword($userDto->password),
                $userDto->name,
                $userDto->surname,
                $userDto->phone,
                null
            );
            $this->userRepository->addUser($user);
        } else {
            throw new WrongDataException("email is taken");
        }
    }

    private function isEmailValid($email)
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    private function isEmailTaken($email): bool
    {
        try {
            $this->userRepository->findByEmail($email);
            return true;
        } catch (NotFoundException $exception) {
            return false;
        }
    }

    public function setUserSession(string $email)
    {
        $_SESSION['email'] = $email;
    }

    public function getHashPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @throws NotFoundException
     */
    public function findUserById(int $id): User
    {
        return $this->userRepository->findById($id);
    }

    public function searchUser(string $search): array
    {
        return $this->mapUsersFromDbToModel($this->userRepository->findUsersByNameOrSurname($search));
    }
}