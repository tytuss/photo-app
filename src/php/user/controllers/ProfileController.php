<?php

require_once 'src/php/user/models/User.php';
require_once 'src/php/user/services/UserService.php';

class ProfileController
{
    private UserService $userService;
    private Response $response;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->response = new Response();
    }

    public function redirectProfilePage(int $userId)
    {
        try {
            $this->userService->findUserById($userId);
            $this->response->renderHtmlFromPhp("src/front/views/profile.php");
        } catch (NotFoundException $e) {
            $this->response->renderHtmlFromPhp("src/front/views/notfoundpage.php");
        }
    }
}