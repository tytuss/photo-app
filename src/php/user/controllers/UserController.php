<?php

require_once 'src/php/user/repositories/UserRepository.php';
require_once 'src/php/user/models/User.php';
require_once 'src/php/user/services/UserService.php';
require_once 'src/php/core/exceptions/WrongDataException.php';

class UserController
{
    private UserService $userService;

    public function __construct()
    {
        $this->userService = new UserService();
    }

    public static function redirectRegisterPage()
    {
        echo file_get_contents("src/front/views/register.html", true);
    }

    public function registerUser($userDto): int
    {
        try {
            $this->userService->createUser($userDto);
            return 201;
        } catch (WrongDataException $e) {
            return 400;
        } catch (Exception $e) {
            return 409;
        }
    }

    /**
     * @throws WrongDataException
     * @throws NotFoundException
     */
    public function signInUser(string $email, string $password): User
    {
        return $this->userService->authorizeUser($email, $password);
    }

    public function logoutCurrentUser()
    {
        $this->userService->logout();
    }

    /**
     * @throws NotFoundException
     */
    public function findUserById(int $userId): User
    {
        return $this->userService->findUserById($userId);
    }

    public function searchUser(string $search): array
    {
        return $this->userService->searchUser($search);
    }

}