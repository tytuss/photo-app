<?php

require_once 'src/php/core/database/Repository.php';
require_once 'src/php/core/exceptions/NotFoundException.php';

class UserRepository extends Repository
{
    /**
     * @throws Exception
     */
    public function addUser(User $user)
    {
        try {
            $this->database->connect()->beginTransaction();
            $stmt = $this->database->connect()->prepare('
                INSERT INTO public.user_details (name, surname, phone)
                VALUES (?, ?, ?)
            ');

            $stmt->execute([
                $user->getName(),
                $user->getSurname(),
                $user->getPhone()
            ]);

            $stmt = $this->database->connect()->prepare('
                INSERT INTO public.user (email, password, id_user_details, id_role, enabled, created_date)
                VALUES (?, ?, ?, ?, ?, NOW())
                ');
            $userRoleId = 1;
            $stmt->execute([
                $user->getEmail(),
                $user->getPassword(),
                $this->getUserDetailsId($user),
                $userRoleId,
                true
            ]);
            $this->database->connect()->inTransaction();
        } catch (PDOException $exception) {
            $this->database->connect()->rollBack();
            throw new Exception("transaction error insert user");
        }
    }

    /**
     * @throws NotFoundException
     */
    public function findByEmail(string $email): User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.user as u INNER JOIN public.user_details as ud ON u.id_user_details = ud.id_user_details WHERE u.email = :email 
        ');
        $stmt->bindParam(':email', $email);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            throw new NotFoundException("user not found");
        }
        return new User($result["id_user"], $result["email"], $result["password"], $result["name"], $result["surname"], $result["phone"], $result["photo_profile"]);
    }

    public function getUserDetailsId(User $user): int
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.user_details WHERE name = :name AND surname = :surname AND phone = :phone
        ');
        $name = $user->getName();
        $surname = $user->getSurname();
        $phone = $user->getPhone();

        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':surname', $surname);
        $stmt->bindParam(':phone', $phone);
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        return $data['id_user_details'];
    }

    /**
     * @throws NotFoundException
     */
    public function findById(int $id): User
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.user as u INNER JOIN public.user_details as ud ON u.id_user_details = ud.id_user_details WHERE u.id_user = :id 
        ');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        if (!$result) {
            throw new NotFoundException("user not found");
        }
        return new User($result["id_user"], $result["email"], $result["password"], $result["name"], $result["surname"], $result["phone"], $result["photo_profile"]);
    }

    public function findUsersByNameOrSurname(string $search): array
    {
        $search = '%' . strtolower($search) . '%';
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.user as u INNER JOIN public.user_details as ud ON u.id_user_details = ud.id_user_details
            WHERE LOWER(ud.name) LIKE :search OR LOWER(ud.surname) LIKE :search LIMIT 5
        ');
        $stmt->bindParam(':search', $search);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}