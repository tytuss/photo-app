<?php


class Comment implements JsonSerializable
{
    private int $postId;
    private User $author;
    private string $message;
    private string $createdDate;


    public function __construct(int $postId, User $author, string $message, string $createdDate)
    {
        $this->postId = $postId;
        $this->author = $author;
        $this->message = $message;
        $this->createdDate = $createdDate;
    }

    /**
     * @return int
     */
    public function getPostId(): int
    {
        return $this->postId;
    }

    /**
     * @param int $postId
     */
    public function setPostId(int $postId): void
    {
        $this->postId = $postId;
    }

    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }

    /**
     * @param User $author
     */
    public function setAuthor(User $author): void
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->createdDate;
    }

    /**
     * @param string $createdDate
     */
    public function setCreatedDate(string $createdDate): void
    {
        $this->createdDate = $createdDate;
    }


    public function jsonSerialize(): array
    {
        return [
            'id_post' => $this->getPostId(),
            'message' => $this->getMessage(),
            'author' => array("id" => $this->author->getId(),
                "name" => $this->author->getName(),
                "surname" => $this->author->getSurname(),
                "photo" => $this->author->getPhoto()),
        ];
    }
}