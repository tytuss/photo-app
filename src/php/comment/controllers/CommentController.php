<?php

require_once 'src/php/comment/repositories/CommentRepository.php';
require_once 'src/php/user/services/UserService.php';
require_once 'src/php/comment/models/Comment.php';

class CommentController
{
    private CommentRepository $commentRepository;
    private UserService $userService;

    public function __construct()
    {
        $this->userService = new UserService();
        $this->commentRepository = new CommentRepository();
    }

    /**
     * @throws NotFoundException
     */
    function createComment($message, $postId): Comment
    {
        $author = $this->userService->getUserFromSession();
        $comment = new Comment($postId, $author, $message, $this->getTimeNow());
        $this->commentRepository->addComment($comment);
        return $comment;

    }

    private function getTimeNow(): string
    {
        return date('Y-m-d H:i:s', time());
    }
}