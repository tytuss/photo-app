<?php

require_once 'src/php/core/database/Repository.php';

class CommentRepository extends Repository
{
    public function addComment(Comment $comment)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO public.post_comment (id_post, id_user, message, created_date)
            VALUES (:id_post, :id_user, :message, :created_date)
        ');
        $postId = $comment->getPostId();
        $userId = $comment->getAuthor()->getId();
        $message = $comment->getMessage();
        $createdDate = $comment->getCreatedDate();
        $stmt->bindParam(':id_post', $postId);
        $stmt->bindParam(':id_user', $userId);
        $stmt->bindParam(':message', $message);
        $stmt->bindParam(':created_date', $createdDate);
        $stmt->execute();
    }

    public function findLast15ByPostId(int $id): array
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.post_comment as p
                INNER JOIN public.user as u ON p.id_user = u.id_user 
                INNER JOIN public.user_details as ud ON ud.id_user_details = u.id_user_details
                    WHERE p.id_post = :id ORDER BY p.created_date DESC limit 15;
        ');
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result ? $result : array();
    }
}